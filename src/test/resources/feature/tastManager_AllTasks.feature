@regressionTests
Feature:All Tasks Page - Validate all task page

    Scenario: TestAllTasksPage_001 USer should be able to add tasks and see them in all tasks page
        Given The user is on Login Page
        When The user enters valid credential and log in successfully
        Then The user should be able to navigate to the My day page
        And  The user should be able add many tasks
        And  The user navigates to All tasks page to verify tasks

