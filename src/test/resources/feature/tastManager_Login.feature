@regressionTests
Feature:Login Page - Check validation for login page

    Scenario: TestLoginPage_001 User should be able to login with valid credentials
        Given The user is on Login Page
        When The user enters valid credential
        And hits the submit button
        Then The user should be able logged in successfully

    Scenario: TestLoginPage_002 User should not able to login with invalid password
        Given The user is on Login Page
        When The user enters invalid password
        And hits the submit button
        Then The user should not be able to log in

    Scenario: TestLoginPage_003 User should not able to login with invalid user name
        Given The user is on Login Page
        When The user enters invalid username
        And hits the submit button
        Then The user should not be able to log in