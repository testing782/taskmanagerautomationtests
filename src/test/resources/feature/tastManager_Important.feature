@regressionTests
Feature:Important Tasks Page - Validate Important page

    Scenario: TestImportantTasksPage_001 User should be able to view all important tasks
        Given The user is on Login Page
        When The user enters valid credential and log in successfully
        Then The user should be able to navigate to the My day page
        And  The user should be able add many tasks
        And  The user navigates to Important tasks page to verify important tasks

