@regressionTests
Feature:May Day Page - Check validation for My day page

  Scenario: TestMyDayPage_001 User should be able to login with valid credentials and My day page should appear
    Given The user is on Login Page
    When The user enters valid credential and log in successfully
    Then The user should be able to navigate to the My day page

  Scenario: TestMyDayPage_002 User should be able to login with valid credentials, can add and remove tasks
    Given The user is on Login Page
    When The user enters valid credential and log in successfully
    Then The user should be able to navigate to the My day page
    And  The user should be able add and remove tasks

  Scenario: TestMyDayPage_003 User should be able to login with valid credentials and can mark/unmark a task
    Given The user is on Login Page
    When The user enters valid credential and log in successfully
    Then The user should be able to navigate to the My day page
    And  The user should be able add task
    And  The user should be able to mark and unmark a task