@regressionTests
Feature:Logout functionality - User should able to logout from the application

    Scenario: TestLogout_001 User should be able to logout from the application
        Given The user is on Login Page
        When The user enters valid credential
        And hits the submit button
        Then The user should be able logged in successfully
        And Then user should be able to logout
