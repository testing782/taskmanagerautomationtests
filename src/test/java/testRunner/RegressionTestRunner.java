package testRunner;

import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
           glue = {"StepDefinitions"},
           plugin = {"json:target/cucumber-report/cucumber.json"},
           tags = "not @skip",
           //tags = "@focus",
           features = {"target/test-classes/feature"}
)

public class RegressionTestRunner extends AbstractTestNGCucumberTests {

}
