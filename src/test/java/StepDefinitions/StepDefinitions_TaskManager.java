package StepDefinitions;

import checksAssertions.HomePageChecks;
import helperUtils.DataUtils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import taskManager.pages.TaskManagerLoginPage;
import taskManager.pages.TaskManagersMyDayPage;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static helperUtils.DataUtils.readLoginData;
import static helperUtils.HelperUtils.getTodaysDateInGivenFormat;
import static helperUtils.WebDriverUtils.webdriver;
import static helperUtils.WebDriverUtils.ngdriver;


public class StepDefinitions_TaskManager {

    TaskManagerLoginPage loginPage;

    @Before
      public void beforeSenario(Scenario scenario){
        System.out.println("Before");
        loginPage = new TaskManagerLoginPage();
        loginPage.setup();

        Assert.assertEquals(webdriver.getCurrentUrl(), "http://localhost:4200/login" );
        String titlePage = webdriver.getTitle();
        Assert.assertEquals(titlePage, "Task Manager" );

    }

    @After
    public void afterScenario(Scenario scenario){
        System.out.println("After: "+ scenario.getName());


        if(scenario.isFailed()){
            try{
            System.out.println("Failed");
//            loginPage = new TaskManagerLoginPage();
//            webdriver.close();
            }catch (WebDriverException wde){

                System.err.println(wde.getMessage());

            } catch (ClassCastException cce){

                cce.printStackTrace();
            }

        }
        ngdriver.waitForAngularRequestsToFinish();
        webdriver.quit();
    }


    @Given("The user is on Login Page")
    public void The_user_is_on_Login_Page(){
          System.out.println("in Login page");
          Assert.assertEquals(webdriver.getCurrentUrl(), "http://localhost:4200/login" );
    }

    @When("The user enters valid credential")
    public void the_user_enters_valid_credential() throws IOException{
        System.out.println("The user enters valid credential");

        String[] login = readLoginData("valid");
        loginPage.inputCredentials(login[1], login[2]);
        }

    @When("The user enters invalid password")
    public void the_user_enters_invalid_credential() throws IOException {

        String[] login = readLoginData("invalidPassword");
        loginPage.inputCredentials(login[1], login[2]);
    }

    @When("hits the submit button")
    public void hits_the_submit_button() {
        loginPage.submitLoginForm();
        System.out.println("hits the submit button");
    }
    @Then("The user should be able logged in successfully")
    public void the_user_should_be_able_logged_in_successfully() {
        Assert.assertEquals(webdriver.getCurrentUrl(), "http://localhost:4200/nav/home" );

    }

    @Then("The user should not be able to log in")
    public void the_user_should_not_able_logged_in() {
        Assert.assertEquals(webdriver.getCurrentUrl(), "http://localhost:4200/login" );
        System.out.println("tag name: " + webdriver.findElement(new By.ByTagName("mat-error")).getText());
    }

    @When("The user enters invalid username")
    public void theUserEntersInvalidUsername() throws IOException{
        String[] login = readLoginData("invaliUserName");
        loginPage.inputCredentials(login[1], login[2]);
    }
    @When("The user enters valid credential and log in successfully")
    public void theUserEntersValidCredentialAndLogInSuccessfully() throws IOException{

        String[] login = readLoginData("valid");
        loginPage.inputCredentials(login[1], login[2]);
        loginPage.submitLoginForm();
    }

    @And("The user should be able add and remove tasks")
    public void theUserShouldBeAbleAddAndRemoveTasks() throws ParseException {
        System.out.println("In theUserShouldBeAbleAddAndRemoveTasks");
        //check before adding any tasks that a single default task exists
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 1);

        //todo remove hard coding
        //add values
        String taskTitle = "Test";
        String taskDesc = "Testing";

        Boolean taskImportant =true ;
        TaskManagersMyDayPage.addTaskInputs(taskTitle,taskDesc, getTodaysDateInGivenFormat("MM/dd/yyyy"),taskImportant);

        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[ng-reflect-model=\""+ taskTitle+"\"]")).size(),1);
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[ng-reflect-model=\""+ taskDesc+"\"]")).size(),1);

        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[ng-reflect-name=\"taskDate\"]")).get(0).getAttribute("ng-reflect-model"),getTodaysDateInGivenFormat("EEE MMM dd yyyy").trim() + " 00:00:00 GMT+0");
        TaskManagersMyDayPage.clickAddTaskButton();

        //check now there are two tasks
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 2);

        //delete the last taks - because we know the new task added will be the last task
        //in real scenario testing will involve more testing around this feature
        TaskManagersMyDayPage.removeLastTask();

        //check after deleting the last task
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 1);

    }

    @Then("The user should be able to navigate to the My day page")
    public void theUserShouldBeAbleToNavigateToTheMyDayPage() {
        System.out.println("theUserShouldBeAbleToNavigateToTheMyDayPage");
        HomePageChecks.homePageNavigationChecks();
    }

    @And("The user should be able add task")
    public void theUserShouldBeAbleAddTask() throws ParseException {
        System.out.println("In theUserShouldBeAbleAddTask");
        //check before adding any tasks that a single default task exists
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 1);

        //todo remove hard coding
        //add values
        String taskTitle = "Test";
        String taskDesc = "Testing";

        Boolean taskImportant= true;
        TaskManagersMyDayPage.addTaskInputs(taskTitle,taskDesc,getTodaysDateInGivenFormat("MM/dd/yyyy"),taskImportant);

        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[ng-reflect-model=\""+ taskTitle+"\"]")).size(),1);
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[ng-reflect-model=\""+ taskDesc+"\"]")).size(),1);
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[ng-reflect-name=\"taskDate\"]")).get(0).getAttribute("ng-reflect-model"),getTodaysDateInGivenFormat("EEE MMM dd yyyy").trim() + " 00:00:00 GMT+0");
        TaskManagersMyDayPage.clickAddTaskButton();

        //check now there are two tasks
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 2);

    }


    @And("The user should be able to mark and unmark a task")
    public void theusershouldbeabletomarkandunmarkatask() throws InterruptedException {

        //check there are two tasks
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 2);

        WebElement lastAngularCheckBox =  webdriver.findElements(By.tagName("mat-checkbox")).get(2);
        WebElement hiddenCheckBox= lastAngularCheckBox.findElement(By.tagName("input"));

        Assert.assertEquals(lastAngularCheckBox.getAttribute("ng-reflect-checked"),"false");
        Assert.assertEquals(hiddenCheckBox.getAttribute("type"),"checkbox");

        TaskManagersMyDayPage.toggleCheckBox(lastAngularCheckBox);
        Assert.assertEquals(lastAngularCheckBox.getAttribute("ng-reflect-checked"),"true");
        Assert.assertEquals(hiddenCheckBox.getAttribute("type"),"checkbox");

        TaskManagersMyDayPage.toggleCheckBox(lastAngularCheckBox);
        Assert.assertEquals(lastAngularCheckBox.getAttribute("ng-reflect-checked"),"false");
        Assert.assertEquals(hiddenCheckBox.getAttribute("type"),"checkbox");
        Assert.assertEquals(hiddenCheckBox.getAttribute("checked"),"true");

     }

    @And("The user should be able add many tasks")
    public void theUserShouldBeAbleAddManyTasks() throws IOException, ParseException {

        ArrayList<String[]> tasks = DataUtils.readTasksData();
        System.out.println("tasks : "+ tasks);

        for (String[] taskLine : tasks){
            //TaskManagersMyDayPage.addTaskInputs(taskLine[0],taskLine[1],taskLine[2],taskLine[3] );
            TaskManagersMyDayPage.addTaskInputs(taskLine[0],taskLine[1],taskLine[2], Boolean.valueOf(taskLine[3]));
            TaskManagersMyDayPage.clickAddTaskButton();
        }
    }

    @And("The user navigates to All tasks page to verify tasks")
    public void theUserNavigatesToAllTasksPageToVerifyTasks() {
        //navigate to all tasks page
        List<WebElement> allTasksLink = webdriver.findElements(new By.ByCssSelector("[ng-reflect-router-link=\"all-tasks\"]"));
        allTasksLink.get(0).findElements(By.xpath("div/div")).get(3).click();

        Assert.assertEquals(webdriver.getCurrentUrl(), "http://localhost:4200/nav/all-tasks" );
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 6);

    }

    @And("The user navigates to Important tasks page to verify important tasks")
    public void theUserNavigatesToImportantTasksPageToVerifyImportantTasks() {

        //navigate to important tasks page
        List<WebElement> importantTasksLink = webdriver.findElements(new By.ByCssSelector("[ng-reflect-router-link=\"important-tasks\"]"));
        Assert.assertEquals(importantTasksLink.size(), 1);
        importantTasksLink.get(0).findElements(By.xpath("div/div")).get(3).click();

        Assert.assertEquals(webdriver.getCurrentUrl(), "http://localhost:4200/nav/important-tasks" );
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]")).size(), 2);

    }
    @And("Then user should be able to logout")
    public void thenUserShouldBeAbleToLogout() {
        //find logout button
        Assert.assertEquals(webdriver.findElements(new By.ByCssSelector("[class=\"mat-icon notranslate material-icons mat-icon-no-color\"]")).size(), 5);
        List<WebElement> logoutButton =webdriver.findElements(new By.ByCssSelector("[class=\"mat-icon notranslate material-icons mat-icon-no-color\"]"));
        logoutButton.get(4).click();
        Assert.assertEquals(webdriver.getCurrentUrl(), "http://localhost:4200/login" );
    }
}
