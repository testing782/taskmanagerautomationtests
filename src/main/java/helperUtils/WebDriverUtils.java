package helperUtils;

import com.paulhammant.ngwebdriver.NgWebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverUtils {

   public static WebDriver webdriver;
   public static NgWebDriver ngdriver;
   static JavascriptExecutor javascriptExecutor;

   //todo Ideally the variable should be exposed as getter/setter methods but I did not do it to save time

//    public static WebDriver getWebDriver (){
//        return webdriver;
//    }
//    public static NgWebDriver getNgDriver (){
//        return ngdriver;
//    }

    public static void setup(){
        System.out.println("In TaskManagerLoginPageTest startApplication");
        WebDriverManager.chromedriver().setup();
        webdriver = new ChromeDriver();
        javascriptExecutor = (JavascriptExecutor)webdriver;
        ngdriver = new NgWebDriver(javascriptExecutor);
    }
}
