package helperUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HelperUtils {

    public static String getTodaysDateInGivenFormat(String dateFormat){
        Date today = new Date();
        SimpleDateFormat todaysDateCSSFormat = new SimpleDateFormat(dateFormat);
        return todaysDateCSSFormat.format(today);
    }


}
