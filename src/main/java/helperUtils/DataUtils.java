package helperUtils;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


public class DataUtils {

static List<String> loginData;
//static List<String> tasksData;
static ArrayList<String[]> tasksData;

    //returns login username and password for the test scenarios requested e.g. will return valid username and password if login data type is valid
    //if login data from csv is store in static object, if object is null data will be read from file.
    public static String[] readLoginData(String loginDataType) throws IOException {

        System.out.println("loginDataType: " + loginDataType);
        if (loginData == null)
        {
            loginData = readLoginData();
        }
        //for saving time I am not adding arrary size and other checks to avoid any errors
        for(String line : loginData)
        {
              String[] lineDataArray= line.split(",");

            if(lineDataArray[0].compareToIgnoreCase(loginDataType)==0 ){
               return lineDataArray;
            }

        }

        return null;
    }

    public static ArrayList<String[]> readTasksData() throws IOException {

        System.out.println("read Tasks data ");
        if (tasksData == null)
        {
            //todo remove hard coding
            String csvFile = "src/test/resources/testdata/createTasks.csv";
            System.out.println("csvfile: " + csvFile);
            List<String> tasksDatList =  readCSV(csvFile);

           tasksData = new ArrayList<String[]>();

            for(String line : tasksDatList)
            {

               tasksData.add(line.split(","));

            }
            // remove header row
           tasksData.remove(0);
        }
        System.out.println("tasksData:" + tasksData);
        return tasksData;
    }

    public static List<String> readLoginData() throws IOException {
        //todo remove hard coding
        System.out.println("read logindata ");
        String csvFile = "src/test/resources/testdata/getUserDetails.csv";
        System.out.println("csvfile: " + csvFile);
        return readCSV(csvFile);

    }

    //generic method to read a csv file
    public static List<String> readCSV(String csvFile) throws IOException {
        System.out.println("in readcsv");
        File file = new File(csvFile);
        List<String> csvData = FileUtils.readLines(file, Charset.defaultCharset());
        System.out.println(csvData);
        return csvData;
}

}
