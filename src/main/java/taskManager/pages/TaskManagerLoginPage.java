package taskManager.pages;

import com.paulhammant.ngwebdriver.ByAngularModel;
import helperUtils.WebDriverUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static helperUtils.WebDriverUtils.webdriver;

public class TaskManagerLoginPage {
    public TaskManagerLoginPage(){
            System.out.println("In TaskManagerLoginPageTest Constructor");
        }
    public void setup(){

        System.out.println("In TaskManagerLoginPageTest startApplication");

        WebDriverUtils.setup();
        //todo remove hardcoding
        webdriver.get("http://localhost:4200/login");
        WebDriverUtils.ngdriver.waitForAngularRequestsToFinish();
    }
    public void inputCredentials(String userName, String password) throws IOException {

        WebElement emaiInput = webdriver.findElement(ByAngularModel.id("mat-input-0"));
        emaiInput.sendKeys(userName);

        WebElement passwordInput = webdriver.findElement(ByAngularModel.id("mat-input-1"));
        passwordInput.sendKeys(password);

    }
    public void submitLoginForm()
    {
        WebElement loginButton = webdriver.findElement(new By.ByTagName("button"));
        loginButton.click();
    }

    }





