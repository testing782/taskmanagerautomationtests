package taskManager.pages;


import helperUtils.WebDriverUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static helperUtils.WebDriverUtils.webdriver;

public class TaskManagersMyDayPage {
    //add a task
    public static void addTaskInputs(String taskTitle, String taskDesc, String taskDueDate, Boolean taskImportant) throws ParseException {

        webdriver.findElement(new By.ByCssSelector("[name=\"taskTitle\"]")).sendKeys(taskTitle);

        webdriver.findElement(new By.ByCssSelector("[name=\"taskDesc\"]")).sendKeys(taskDesc);

        if ((taskDueDate != null)
                && !taskDueDate.trim().isEmpty()) {

            webdriver.findElement(new By.ByCssSelector("[class=\"mat-datepicker-toggle-default-icon ng-star-inserted\"]")).click();

            //assumption date format is always correct
            Date taskDueDateObj = new SimpleDateFormat("MM/dd/yyyy").parse(taskDueDate);
            SimpleDateFormat taskDueDateCSSFormat = new SimpleDateFormat("MMMM dd, yyyy");
            String taskDueDateCssSelector = "[aria-label=\"" + taskDueDateCSSFormat.format(taskDueDateObj) + "\"]";
            webdriver.findElement(new By.ByCssSelector(taskDueDateCssSelector)).click();
        }

        System.out.println("taskImportant" + taskImportant);

        if (taskImportant == true) {
            webdriver.findElement(new By.ByCssSelector("[class=\"mat-checkbox-inner-container\"]")).click();
        }
    }

    public static void clickAddTaskButton() {
        webdriver.findElement(new By.ByCssSelector("[id=\"addTask\"]")).click();
        WebDriverUtils.ngdriver.waitForAngularRequestsToFinish();

    }

    public static void removeLastTask() {
        List<WebElement> taskCards = webdriver.findElements(new By.ByCssSelector("[class=\"home-card task-card mat-card ng-star-inserted\"]"));
        taskCards.get(1).findElement(new By.ByCssSelector("[class=\"remove-icon mat-icon notranslate material-icons mat-icon-no-color\"]")).click();
        WebDriverUtils.ngdriver.waitForAngularRequestsToFinish();

    }

    public static void toggleCheckBox(WebElement checkBox) {
        checkBox.click();
    }

}
