# Read Me
This file provides the summary of the work done and how to run the tests. 

## Pre-Requisites
### Software used to create the test suite
1) Java 1.18
2) Maven 3.8.5
3) Cucumber
4) Intellij IDE
5) Selenium
6) Chrome Browser 102.0.5005.115
7) OS: Windows Server 2022

### Java JDK
-	Download and install Java JDK 18 *(I have not tested the code with any other Java version)*
-	e.g. JAVA_HOME = C:\Program Files\Java\jdk1.8.0_172
-	Set JAVA_HOME environment variable set to JDK 
-	Test JDK installation by typing java -version at the Command Prompt - you should get the java version returned
-	Test Environment variable by typing echo %JAVA_HOME% at the command prompt, you should see your java installation directory returned - check it is the JDK, not the JRE
### Maven Build
- Install Apache Maven 3.8.5 to c:\C:\apache-maven-3.8.5 *(I have not tested the code with any other Maven version)*
- Set M2_HOME environment variable to the Maven installation directory e.g. M2_HOME = C:\apache-maven-3.8.5
- Add to PATH system environment variable, ensure it includes M2_HOME\bin (to allow maven to run from the command prompt)

## How to Run the tests

### Before Running
- Please ensure the TaskManager app is running before the following commands are executed.
- The test use Chrome driver please make sure Chrome is installed before executing the tests
- I have added a small batch file **RunTest.bat** with the commands to run the tests.
 
### From the Command Line 

- Execute the Type **RunTest.bat** (on the command line) from the root folder (from the same folder where pom.xml exists) 
- **OR** Execute the following command
- `mvn clean test cluecumber-report:reporting "-Dcucumber.options=--tags @regressionTests"`

### From IntelliJ**
- Run `functionalTest_TaskManager @regressionTests - maven configuration`  

## Test Report
- I have used the **Cluecumber Reporting** plugin.
- Test Report location/link will be shown on the console at the end of run. 
- The test report files will most likely exists in **generated-report** folder under the **target** folder. Please open the **index.html** to see the summary.
- Reports are self-explanatory

## Observations/Defects
- **Important Page** some time does not show all important tasks after adding.
- Apart from the **My Day** page, no other page shows the  Task Due Date.
- Task Description is never shown in any of the pages.
- There is no way to delete tasks other than from the **My Day** page. The page only shows tasks due today or tasks without any due date.
- There is no way to mark or unmark tasks as important other than from the **My Day** page.

## What was done 
- Representative tests for each given requirements *(in the TaskUI Readme file)* are covered.
- Added few more negative and positive tests for login page and all tasks pages.

## What was not done
- Because I was working on this problem after hours only, I had time boxed the work by the end of the weekend hence I prioritised finising some representative tasks for each requirement rather than going in detail for each test scenario.
- I have not covered extensive testing for the negative scenarios - 
- Many Other adhoc test like Browser back button tests were not covered.
- More tests can be added for the **Login** page - e.g. sql injections test, add very long text in the field, add some special character tests.
- More tests can be added for the **All Tasks** page for dates test. Like future date or very old date checks are not added. 
- Multiple browser tests are not covered.
- My code has some hardcoding which was not removed due to time constraints.
- Some patterns are not consistently applied due to time constraints.
